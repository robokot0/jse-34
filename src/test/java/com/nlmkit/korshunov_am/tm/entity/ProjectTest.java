package com.nlmkit.korshunov_am.tm.entity;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ProjectTest {

    @Test
    public void getIdsetId() {
        Project project = new Project();
        project.setId(Long.MAX_VALUE);
        assertEquals(project.getId().longValue(),Long.MAX_VALUE);
    }

    @Test
    public void getNamesetName() {
        Project project = new Project();
        project.setName("name");
        assertEquals(project.getName(),"name");
    }

    @Test
    public void getDescriptionsetDescription() {
        Project project = new Project();
        project.setDescription("aaa");
        assertEquals(project.getDescription(),"aaa");
    }

    @Test
    public void getUserIdsetUserId() {
        Project project = new Project();
        project.setUserId(Long.MAX_VALUE);
        assertEquals(project.getUserId().longValue(),Long.MAX_VALUE);
    }

    @Test
    public void testToString() {
        Project project = new Project("name",Long.MAX_VALUE);
        assertEquals(project.toString(),project.getId() + ": " + project.getName());
    }
}