package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProjectTaskServiceTest {
    static ProjectTaskService projectTaskService;
    static ProjectRepository projectRepository;
    static TaskRepostory taskRepostory;

    @BeforeAll
    public static void doBeforeAll() {
        projectRepository = Mockito.mock(ProjectRepository.class);
        taskRepostory = Mockito.mock(TaskRepostory.class);
        projectTaskService = new ProjectTaskService(projectRepository,taskRepostory);
    }

    @Test
    void getInstance() {
        ProjectTaskService projectTaskService = ProjectTaskService.getInstance();
        assertEquals(projectTaskService,ProjectTaskService.getInstance());
    }

    @Test
    void findAllByProjectId() {
        assertDoesNotThrow(()->{
            projectTaskService.findAllByProjectId(0L);
            verify(taskRepostory,times(1)).findAllByProjectId(0L);
        });
        assertThrows(WrongArgumentException.class,()-> projectTaskService.findAllByProjectId(null));
    }

    @Test
    void testFindAllByProjectId() {
        assertDoesNotThrow(()->{
            assertNotNull(projectTaskService.findAllByProjectId(0L,0L));
            verify(taskRepostory,times(1)).findAllByProjectId(0L,0L);
        });
        assertThrows(WrongArgumentException.class,()-> projectTaskService.findAllByProjectId(null,0L));
    }

    @Test
    void removeTaskFromProject() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0L, arg0.longValue());
            assertEquals(1L, arg1.longValue());
            return null;
        }).when(taskRepostory).findByProjectIdAndId(0L,1L);
        doReturn(null).when(taskRepostory).findByProjectIdAndId(0L,1L);
        Task task = Mockito.mock(Task.class);
        Mockito.doAnswer(inv->{
            Long arg0 = inv.getArgument(0);
            assertNull(arg0);
            return null;
        }).when(task).setProjectId(null);
        doReturn(task).when(taskRepostory).findByProjectIdAndId(1L,1L);
        assertThrows(WrongArgumentException.class,()-> projectTaskService.removeTaskFromProject(null,0L));
        assertThrows(WrongArgumentException.class,()-> projectTaskService.removeTaskFromProject(0L,null));
        assertThrows(TaskNotFoundException.class,()-> projectTaskService.removeTaskFromProject(0L,1L));
        assertDoesNotThrow(()-> assertNotNull(projectTaskService.removeTaskFromProject(1L,1L)));
    }

    @Test
    void testRemoveTaskFromProject() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            Long arg2 = inv.getArgument(2);
            assertEquals(0L, arg0.longValue());
            assertEquals(1L, arg1.longValue());
            assertEquals(0L, arg2.longValue());
            return null;
        }).when(taskRepostory).findByProjectIdAndId(0L,1L,0L);
        doReturn(null).when(taskRepostory).findByProjectIdAndId(0L,1L,0L);
        Task task = Mockito.mock(Task.class);
        Mockito.doAnswer(inv->{
            Long arg0 = inv.getArgument(0);
            assertNull(arg0);
            return null;
        }).when(task).setProjectId(null);
        doReturn(task).when(taskRepostory).findByProjectIdAndId(1L,1L,0L);
        assertThrows(WrongArgumentException.class,()-> projectTaskService.removeTaskFromProject(null,0L,0L));
        assertThrows(WrongArgumentException.class,()-> projectTaskService.removeTaskFromProject(0L,null,0L));
        assertThrows(WrongArgumentException.class,()-> projectTaskService.removeTaskFromProject(0L,0L,null));
        assertThrows(TaskNotFoundException.class,()-> projectTaskService.removeTaskFromProject(0L,1L,0L));
        assertDoesNotThrow(()-> assertNotNull(projectTaskService.removeTaskFromProject(1L,1L,0L)));
    }

    @Test
    void addTaskToProject() {
        assertThrows(WrongArgumentException.class,()-> projectTaskService.addTaskToProject(null,0L));
        assertThrows(WrongArgumentException.class,()-> projectTaskService.addTaskToProject(0L,null));

        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0L, arg0.longValue());
            assertEquals(1L, arg1.longValue());
            return null;
        }).when(projectRepository).findById(0L);
        doReturn(null).when(projectRepository).findById(0L);
        Project project = new Project();
        project.setId(2L);
        doReturn(project).when(projectRepository).findById(2L);
        assertThrows(ProjectNotFoundException.class,()-> projectTaskService.addTaskToProject(0L,1L));
        assertThrows(TaskNotFoundException.class,()-> projectTaskService.addTaskToProject(2L,1L));
        Task task = Mockito.mock(Task.class);
        doReturn(task).when(taskRepostory).findById(3L);
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            assertEquals(2L, arg0.longValue());
            return null;
        }).when(task).setProjectId(2L);
        assertDoesNotThrow(()-> assertNotNull(projectTaskService.addTaskToProject(2L,3L)));
    }

    @Test
    void testAddTaskToProject() {
        assertThrows(WrongArgumentException.class,()-> projectTaskService.addTaskToProject(null,0L,0L));
        assertThrows(WrongArgumentException.class,()-> projectTaskService.addTaskToProject(0L,null,0L));
        assertThrows(WrongArgumentException.class,()-> projectTaskService.addTaskToProject(0L,0L,null));

        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            Long arg2 = inv.getArgument(2);
            assertEquals(0L, arg0.longValue());
            assertEquals(1L, arg1.longValue());
            assertEquals(0L, arg2.longValue());
            return null;
        }).when(projectRepository).findById(0L,0L);
        doReturn(null).when(projectRepository).findById(0L,0L);
        Project project = new Project();
        project.setId(2L);
        doReturn(project).when(projectRepository).findById(2L,0L);
        assertThrows(ProjectNotFoundException.class,()-> projectTaskService.addTaskToProject(0L,1L,0L));
        assertThrows(TaskNotFoundException.class,()-> projectTaskService.addTaskToProject(2L,1L,0L));
        Task task = Mockito.mock(Task.class);
        doReturn(task).when(taskRepostory).findById(3L,0L);
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            assertEquals(2L, arg0.longValue());
            return null;
        }).when(task).setProjectId(2L);
        assertDoesNotThrow(()-> assertNotNull(projectTaskService.addTaskToProject(2L,3L,0L)));
    }
}