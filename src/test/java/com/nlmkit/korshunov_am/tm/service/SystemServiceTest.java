package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class SystemServiceTest {
    static SystemService systemService ;
    static ProjectRepository projectRepository = Mockito.mock(ProjectRepository.class);
    static UserRepository userRepository = Mockito.mock(UserRepository.class);
    static TaskRepostory taskRepostory = Mockito.mock(TaskRepostory.class);
    static UserService userService = Mockito.mock(UserService.class);

    @BeforeAll
    static void doBeforeAll() {
        systemService=new SystemService(projectRepository,userRepository,taskRepostory,userService);
    }


    @Test
    void getInstance() {
        SystemService systemService = SystemService.getInstance();
        assertEquals(systemService,SystemService.getInstance());
    }

    @Test
    void loadTestData() {
        Mockito.doAnswer(inv -> null).when(projectRepository).clear();
        Mockito.doAnswer(inv -> null).when(userRepository).clear();
        Mockito.doAnswer(inv -> null).when(taskRepostory).clear();
        User user = new User();
        user.setId(1L);
        Mockito.doReturn(user).when(userRepository).create(Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any());
        Mockito.doReturn("").when(userService).getStringHash(Mockito.any());
        Project project = new Project();
        project.setId(1L);
        Mockito.doReturn(project).when(projectRepository).create(Mockito.any(),Mockito.any(),Mockito.any());
        Task task = Mockito.mock(Task.class);
        Mockito.doReturn(task).when(taskRepostory).create(Mockito.any(),Mockito.any(),Mockito.any());
        Mockito.doAnswer(inv -> null).when(taskRepostory).findAddByProjectId(Mockito.any(),Mockito.any());
        assertDoesNotThrow(()->systemService.loadTestData());
    }
}