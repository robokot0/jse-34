package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TaskServiceTest {
    static TaskRepostory taskRepostory;
    static TaskService taskService;

    @BeforeAll
    public static void doBeforeAll() {
        taskRepostory = Mockito.mock(TaskRepostory.class);
        taskService = new TaskService(taskRepostory);
    }

    @Test
    void getInstance() {
        TaskService taskService = TaskService.getInstance();
        assertEquals(taskService,TaskService.getInstance());
    }

    @Test
    void create() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals("name", arg0);
            assertEquals(1L, arg1.longValue());
            return null;
        }).when(taskRepostory).create("name",1L);
        assertDoesNotThrow(()->{taskService.create("name",1L);});
        assertThrows(WrongArgumentException.class,()-> taskService.create(null,1L));
        assertThrows(WrongArgumentException.class,()-> taskService.create("",1L));
        assertThrows(WrongArgumentException.class,()-> taskService.create("name",null));
    }

    @Test
    void testCreate() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            String arg1 = inv.getArgument(1);
            Long arg2 = inv.getArgument(2);
            assertEquals("name", arg0);
            assertEquals("description", arg1);
            assertEquals(1L, arg2.longValue());
            return null;
        }).when(taskRepostory).create("name","description",1L);
        assertDoesNotThrow(()->{taskService.create("name","description",1L);});
        assertThrows(WrongArgumentException.class,()-> taskService.create(null,"description",1L));
        assertThrows(WrongArgumentException.class,()-> taskService.create("","description",1L));
        assertThrows(WrongArgumentException.class,()-> taskService.create("name",null,1L));
        assertThrows(WrongArgumentException.class,()-> taskService.create("name","",1L));
        assertThrows(WrongArgumentException.class,()-> taskService.create("name","description",null));
    }

    @Test
    void update() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            String arg1 = inv.getArgument(1);
            String arg2 = inv.getArgument(2);
            Long arg3 = inv.getArgument(3);
            assertEquals(2L, arg0.longValue());
            assertEquals("name", arg1);
            assertEquals("description", arg2);
            assertEquals(1L, arg3.longValue());
            return null;
        }).when(taskRepostory).update(2L,"name","description",1L);
        assertDoesNotThrow(()->{taskService.update(2L,"name","description",1L);});
        assertThrows(WrongArgumentException.class,()-> taskService.update(2L,null,"description",1L));
        assertThrows(WrongArgumentException.class,()-> taskService.update(2L,"","description",1L));
        assertThrows(WrongArgumentException.class,()-> taskService.update(2L,"name",null,1L));
        assertThrows(WrongArgumentException.class,()-> taskService.update(2L,"name","",1L));
        assertThrows(WrongArgumentException.class,()-> taskService.update(2L,"name","description",null));
    }

    @Test
    void clear() {
        taskService.clear();
        verify(taskRepostory,Mockito.times(1)).clear();
    }

    @Test
    void testClear() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            assertEquals(2L, arg0.longValue());
            return null;
        }).when(taskRepostory).clear(2L);
        assertDoesNotThrow(()-> taskService.clear(2L));
        assertThrows(WrongArgumentException.class,()->taskService.clear(null));
    }

    @Test
    void findByIndex() {
        Mockito.doAnswer(inv -> {
            Integer arg0 = inv.getArgument(0);
            assertEquals(0, arg0.intValue());
            return null;
        }).when(taskRepostory).findByIndex(0);
        assertThrows(WrongArgumentException.class,()->taskService.findByIndex(null,false));
        assertThrows(WrongArgumentException.class,()->taskService.findByIndex(-1,false));
        assertThrows(TaskNotFoundException.class,()->taskService.findByIndex(0,true));
        Mockito.doReturn(new Task()).when(taskRepostory).findByIndex(0);
        assertDoesNotThrow(()->{
            verify(taskRepostory,Mockito.times(1)).findByIndex(0);
            taskService.findByIndex(0,false);
        });
    }

    @Test
    void testFindByIndex() {
        Mockito.doAnswer(inv -> {
            Integer arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0, arg0.intValue());
            assertEquals(0, arg1.longValue());
            return null;
        }).when(taskRepostory).findByIndex(0,0L);
        assertDoesNotThrow(()->{taskService.findByIndex(0,0L,false);});
        assertThrows(WrongArgumentException.class,()->taskService.findByIndex(null,0L,false));
        assertThrows(WrongArgumentException.class,()->taskService.findByIndex(-1,0L,false));
        assertThrows(TaskNotFoundException.class,()->taskService.findByIndex(0,0L,true));
    }

    @Test
    void findByName() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            assertEquals("name", arg0);
            return null;
        }).when(taskRepostory).findByName("name");
        assertDoesNotThrow(()-> assertNull(taskService.findByName("name",false)));
        assertThrows(WrongArgumentException.class,()->taskService.findByName(null,false));
        assertThrows(WrongArgumentException.class,()->taskService.findByName("",false));
        assertThrows(TaskNotFoundException.class,()->taskService.findByName("name",true));
    }

    @Test
    void testFindByName() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals("name", arg0);
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(taskRepostory).findByName("name",0L);
        assertDoesNotThrow(()-> assertNull(taskService.findByName("name",0L,false)));
        assertThrows(WrongArgumentException.class,()->taskService.findByName(null,0L,false));
        assertThrows(WrongArgumentException.class,()->taskService.findByName("",0L,false));
        assertThrows(TaskNotFoundException.class,()->taskService.findByName("name",0L,true));
    }

    @Test
    void findById() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            assertEquals(0L, arg0.longValue());
            return null;
        }).when(taskRepostory).findById(0L);
        assertDoesNotThrow(()->{taskService.findById(0L,false);});
        assertThrows(WrongArgumentException.class,()->taskService.findById(null,false));
        assertThrows(TaskNotFoundException.class,()->taskService.findById(0L,true));
    }

    @Test
    void testFindById() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0L, arg0.longValue());
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(taskRepostory).findById(0L,0L);
        assertDoesNotThrow(()-> assertNull(taskService.findById(0L,0L,false)));
        assertThrows(WrongArgumentException.class,()->taskService.findById(null,0L,false));
        assertThrows(TaskNotFoundException.class,()->taskService.findById(0L,0L,true));
    }

    @Test
    void removeByIndex() {
        Mockito.doAnswer(inv -> {
            Integer arg0 = inv.getArgument(0);
            assertEquals(0, arg0.intValue());
            return null;
        }).when(taskRepostory).removeByIndex(0);
        Mockito.doReturn(new Task()).when(taskRepostory).removeByIndex(1);
        assertDoesNotThrow(()-> assertNotNull(taskService.removeByIndex(1)));
        assertThrows(WrongArgumentException.class,()->taskService.removeByIndex(null));
        assertThrows(WrongArgumentException.class,()->taskService.removeByIndex(-1));
        assertThrows(TaskNotFoundException.class,()->taskService.removeByIndex(0));
    }

    @Test
    void testRemoveByIndex() {
        Mockito.doAnswer(inv -> {
            Integer arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0, arg0.intValue());
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(taskRepostory).removeByIndex(0,0L);
        Mockito.doReturn(new Task()).when(taskRepostory).removeByIndex(1,0L);
        assertDoesNotThrow(()-> assertNotNull(taskService.removeByIndex(1,0L)));
        assertThrows(WrongArgumentException.class,()->taskService.removeByIndex(null,0L));
        assertThrows(WrongArgumentException.class,()->taskService.removeByIndex(-1,0L));
        assertThrows(WrongArgumentException.class,()->taskService.removeByIndex(0,null));
        assertThrows(TaskNotFoundException.class,()->taskService.removeByIndex(0,0L));
    }

    @Test
    void removeById() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            assertEquals(0L, arg0.longValue());
            return null;
        }).when(taskRepostory).removeById(0L);
        Mockito.doReturn(new Task()).when(taskRepostory).removeById(1L);
        assertDoesNotThrow(()->{taskService.removeById(1L);});
        assertThrows(WrongArgumentException.class,()->taskService.removeById(null));
        assertThrows(TaskNotFoundException.class,()->taskService.removeById(0L));
    }

    @Test
    void testRemoveById() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0L, arg0.longValue());
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(taskRepostory).removeById(0L);
        Mockito.doReturn(new Task()).when(taskRepostory).removeById(1L,0L);
        assertDoesNotThrow(()->{taskService.removeById(1L,0L);});
        assertThrows(WrongArgumentException.class,()->taskService.removeById(null,0L));
        assertThrows(WrongArgumentException.class,()->taskService.removeById(0L,null));
        assertThrows(TaskNotFoundException.class,()->taskService.removeById(0L,0L));
    }

    @Test
    void removeByName() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            assertEquals("name", arg0);
            return null;
        }).when(taskRepostory).removeByName("name");
        Mockito.doReturn(new Task()).when(taskRepostory).removeByName("name1");
        assertDoesNotThrow(()->{taskService.removeByName("name1");});
        assertThrows(WrongArgumentException.class,()->taskService.removeByName(null));
        assertThrows(WrongArgumentException.class,()->taskService.removeByName(""));
        assertThrows(TaskNotFoundException.class,()->taskService.removeByName("name"));
    }

    @Test
    void testRemoveByName() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals("name", arg0);
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(taskRepostory).removeByName("name",0L);
        Mockito.doReturn(new Task()).when(taskRepostory).removeByName("name1",0L);
        assertDoesNotThrow(()->{taskService.removeByName("name1",0L);});
        assertThrows(WrongArgumentException.class,()->taskService.removeByName(null,0L));
        assertThrows(WrongArgumentException.class,()->taskService.removeByName("",0L));
        assertThrows(WrongArgumentException.class,()->taskService.removeByName("name",null));
        assertThrows(TaskNotFoundException.class,()->taskService.removeByName("name",0L));
    }

    @Test
    void findAllByProjectId() {
        assertDoesNotThrow(()-> {
            taskService.findAllByProjectId(0L);
            verify(taskRepostory,times(1)).findAllByProjectId(0L);
        });
        assertThrows(WrongArgumentException.class,()->taskService.findAllByProjectId(null));
    }

    @Test
    void testFindAllByProjectId() {
        assertDoesNotThrow(()-> {
            taskService.findAllByProjectId(0L,0L);
            verify(taskRepostory,times(1)).findAllByProjectId(0L,0L);
        });
        assertThrows(WrongArgumentException.class,()->taskService.findAllByProjectId(null,0L));
        assertThrows(WrongArgumentException.class,()->taskService.findAllByProjectId(0L,null));
    }

    @Test
    void findAll() {
        assertDoesNotThrow(()-> {
            taskService.findAll();
            verify(taskRepostory,times(1)).findAll();
        });
    }

    @Test
    void testFindAll() {
        assertDoesNotThrow(()->{taskService.findAll(22L);});
        verify(taskRepostory,times(1)).findAll(22L);
        assertThrows(WrongArgumentException.class,()->taskService.findAll(null));
    }

    @Test
    void findByProjectIdAndId() {
        assertDoesNotThrow(()-> {
            taskService.findByProjectIdAndId(0L,0L,false);
            verify(taskRepostory,times(1)).findByProjectIdAndId(0L,0L);
        });
        assertThrows(WrongArgumentException.class,()->taskService.findByProjectIdAndId(null,0L,false));
        assertThrows(WrongArgumentException.class,()->taskService.findByProjectIdAndId(0L,null,false));
        assertThrows(TaskNotFoundException.class,()->taskService.findByProjectIdAndId(0L,0L,true));
        doReturn(new Task()).when(taskRepostory).findByProjectIdAndId(1L,1L);
        assertDoesNotThrow(()->taskService.findByProjectIdAndId(1L,1L,true));
    }

    @Test
    void testFindByProjectIdAndId() {
        assertDoesNotThrow(()-> {
            taskService.findByProjectIdAndId(0L,0L,0L,false);
            verify(taskRepostory,times(1)).findByProjectIdAndId(0L,0L,0L);
        });
        assertThrows(WrongArgumentException.class,()->taskService.findByProjectIdAndId(null,0L,0L,false));
        assertThrows(WrongArgumentException.class,()->taskService.findByProjectIdAndId(0L,null,0L,false));
        assertThrows(WrongArgumentException.class,()->taskService.findByProjectIdAndId(0L,0L,null,false));
        assertThrows(TaskNotFoundException.class,()->taskService.findByProjectIdAndId(0L,0L,0L,true));
        doReturn(new Task()).when(taskRepostory).findByProjectIdAndId(1L,1L,1L);
        assertDoesNotThrow(()->taskService.findByProjectIdAndId(1L,1L,1L,true));
    }

    @Test
    void saveAs() {
        assertDoesNotThrow(()->{
            taskService.saveAs(any(),any());
            verify(taskRepostory,times(1)).saveAs(any(),any());
        });
    }

    @Test
    void loadFrom() {
        assertDoesNotThrow(()->{
            taskService.loadFrom(any(),any());
            verify(taskRepostory,times(1)).loadFrom(any(),any());
        });

    }
}