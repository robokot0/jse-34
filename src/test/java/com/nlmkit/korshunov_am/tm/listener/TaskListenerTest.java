package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.publisher.InputConsole;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.ProjectTaskService;
import com.nlmkit.korshunov_am.tm.service.TaskService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TaskListenerTest {
    static TaskListener taskListener;
    static TaskService taskService;
    static InputConsole inputConsole;
    static ProjectTaskService projectTaskService;
    static CommandHistoryService commandHistoryService;

    @BeforeAll
    public static void doBeforeAll() {
        commandHistoryService = Mockito.mock(CommandHistoryService.class);
        taskService = Mockito.mock(TaskService.class);
        inputConsole =  Mockito.mock(InputConsole.class);
        projectTaskService =  Mockito.mock(ProjectTaskService.class);
        taskListener = new TaskListener(taskService,projectTaskService,commandHistoryService);
        taskListener.setConsole(inputConsole);
    }

    @Test
    void setUsergetUser() {
        User user = new User("login");
        taskListener.setUser(user);
        assertEquals(user,taskListener.getUser());
    }

    @Test
    void enterStringCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputString("pn",false);
            taskListener.enterStringCommandParameter("pn");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn","pv");
        });
    }

    @Test
    void enterPasswordCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputPassword("pn1",false);
            taskListener.enterPasswordCommandParameter("pn1");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn1","******");
        });
    }

    @Test
    void enterIntegerCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn2",true);
            assertEquals(1,taskListener.enterIntegerCommandParameter("pn2"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn2","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn3",true);
            assertEquals(1,taskListener.enterIntegerCommandParameter("pn3"));
        });
    }

    @Test
    void enterLongCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn4",true);
            assertEquals(1L,taskListener.enterLongCommandParameter("pn4"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn4","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn5",true);
            assertEquals(1L,taskListener.enterLongCommandParameter("pn5"));
        });
    }

    @Test
    void showResult() {
        assertDoesNotThrow(()-> {
            taskListener.ShowResult("result");
            verify(inputConsole,times(1)).showMessage("result");
            verify(commandHistoryService,times(1)).AddCommandResultToLastCommand("result");
        });
    }

    @Test
    void testAuthUser() {
        taskListener.setUser(new User("testAuthUser"));
        assertTrue(taskListener.testAuthUser());
        taskListener.setUser(null);
        assertFalse(taskListener.testAuthUser());
    }

    @Test
    void testAdminUser() {
        User user = new User("testAdminUser");
        user.setRole(Role.ADMIN);
        taskListener.setUser(user);
        assertTrue(taskListener.testAdminUser());
        user.setRole(Role.USER);
        assertFalse(taskListener.testAdminUser());
        taskListener.setUser(null);
        assertFalse(taskListener.testAdminUser());
    }

    @Test
    void getInstance() {
        TaskListener taskListener = TaskListener.getInstance();
        assertEquals(taskListener,TaskListener.getInstance());
    }

    @Test
    void testNotify() {
        assertEquals(-1,taskListener.notify("aaaaaa"));
        assertEquals(0,taskListener.notify(SHORT_TASK_CREATE));
        assertEquals(0,taskListener.notify(TASK_CREATE));
        assertEquals(0,taskListener.notify(SHORT_TASK_CLEAR));
        assertEquals(0,taskListener.notify(TASK_CLEAR));
        assertEquals(0,taskListener.notify(SHORT_TASK_LIST));
        assertEquals(0,taskListener.notify(TASK_LIST));
        assertEquals(0,taskListener.notify(SHORT_TASK_VIEW));
        assertEquals(0,taskListener.notify(TASK_VIEW));
        assertEquals(0,taskListener.notify(SHORT_TASK_REMOVE_BY_ID));
        assertEquals(0,taskListener.notify(TASK_REMOVE_BY_ID));
        assertEquals(0,taskListener.notify(SHORT_TASK_REMOVE_BY_NAME));
        assertEquals(0,taskListener.notify(TASK_REMOVE_BY_NAME));
        assertEquals(0,taskListener.notify(SHORT_TASK_REMOVE_BY_INDEX));
        assertEquals(0,taskListener.notify(TASK_REMOVE_BY_INDEX));
        assertEquals(0,taskListener.notify(SHORT_TASK_UPDATE_BY_INDEX));
        assertEquals(0,taskListener.notify(TASK_UPDATE_BY_INDEX));
        assertEquals(0,taskListener.notify(SHORT_TASK_ADD_TO_PROJECT_BY_IDS));
        assertEquals(0,taskListener.notify(TASK_ADD_TO_PROJECT_BY_IDS));
        assertEquals(0,taskListener.notify(SHORT_TASK_REMOVE_FROM_PROJECT_BY_IDS));
        assertEquals(0,taskListener.notify(TASK_REMOVE_FROM_PROJECT_BY_IDS));
        assertEquals(0,taskListener.notify(SHORT_TASK_LISTS_BY_PROJECT_ID));
        assertEquals(0,taskListener.notify(TASK_LISTS_BY_PROJECT_ID));
        assertEquals(0,taskListener.notify(SHORT_SAVE_TO_JSON));
        assertEquals(0,taskListener.notify(SAVE_TO_JSON));
        assertEquals(0,taskListener.notify(SHORT_SAVE_TO_XML));
        assertEquals(0,taskListener.notify(SAVE_TO_XML));
        assertEquals(0,taskListener.notify(SHORT_HELP));
        assertEquals(0,taskListener.notify(HELP));
        assertEquals(0,taskListener.notify(SHORT_LOAD_FROM_XML));
        assertEquals(0,taskListener.notify(LOAD_FROM_XML));
        assertEquals(0,taskListener.notify(SHORT_LOAD_FROM_JSON));
        assertEquals(0,taskListener.notify(LOAD_FROM_JSON));

        assertDoesNotThrow(()-> {
            Mockito.doThrow(IOException.class).when(taskService).loadFrom(any(),any());
            taskListener.notify(LOAD_FROM_JSON);
        });


    }

    @Test
    void updateTaskByIndex() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);

        Task task = new Task();
        task.setUserId(user.getId());
        task.setId(0L);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,user.getId(),true);
            taskListener.updateTaskByIndex();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,true);
            assertEquals(0,taskListener.updateTaskByIndex());
        });
    }

    @Test
    void removeTaskByName() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);

        assertDoesNotThrow(()->{
            doReturn("tn").when(inputConsole).getNextInputString("task name",false);
            taskListener.removeTaskByName();

            user.setRole(Role.ADMIN);
            doReturn("tn").when(inputConsole).getNextInputString("task name",false);
            taskListener.removeTaskByName();
        });

    }

    @Test
    void removeTaskByID() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            taskListener.removeTaskByID();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            taskListener.removeTaskByID();
        });
    }

    @Test
    void removeTaskByIndex() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            taskListener.removeTaskByIndex();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            taskListener.removeTaskByIndex();
        });
    }

    @Test
    void createTask() {
        taskListener.setUser(new User("user"));
        assertDoesNotThrow(()-> {
            doReturn("tn").when(inputConsole).getNextInputString("task name",false);
            doReturn("td").when(inputConsole).getNextInputString("task description",false);
            assertEquals(0, taskListener.createTask());
            verify(taskService,times(1)).create("tn","td",taskListener.getUser().getId());
        });
    }

    @Test
    void clearTask() {
        User user = new User("user");
        user.setRole(Role.USER);

        assertDoesNotThrow(()->{
            taskListener.setUser(null);
            taskListener.clearTask();

            taskListener.setUser(user);
            taskListener.clearTask();

            user.setRole(Role.ADMIN);
            taskListener.clearTask();
        });
    }

    @Test
    void viewTaskByIndex() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);

        Task task = new Task();
        task.setUserId(user.getId());
        task.setId(0L);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,user.getId(),true);
            taskListener.viewTaskByIndex();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,true);
            taskListener.viewTaskByIndex();
        });
    }

    @Test
    void listTask() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task());
        assertDoesNotThrow(()->{
            doReturn(tasks).when(taskService).findAll();
            taskListener.listTask();

            user.setRole(Role.ADMIN);
            taskListener.listTask();
        });

    }

    @Test
    void listTaskByProjectId() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task());
        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project ID",true);
            doReturn(tasks).when(taskService).findAllByProjectId(1L);
            taskListener.listTaskByProjectId();

            user.setRole(Role.ADMIN);
            taskListener.listTaskByProjectId();
        });
    }

    @Test
    void addTaskToProjectByIds() {
        assertDoesNotThrow(()->{
        doReturn("1").when(inputConsole).getNextInputString("project ID",true);
        doReturn("1").when(inputConsole).getNextInputString("task ID",true);
        taskListener.addTaskToProjectByIds();
        verify(projectTaskService,times(1)).addTaskToProject(1L,1L);
        });
    }

    @Test
    void removeTaskFromProjectByIds() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);
        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project ID",true);
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            taskListener.removeTaskFromProjectByIds();

            doReturn("1").when(inputConsole).getNextInputString("project ID",true);
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            user.setRole(Role.ADMIN);
            taskListener.removeTaskFromProjectByIds();
        });
    }

    @Test
    void setTaskUserById() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);
        user.setRole(Role.ADMIN);

        Task task = new Task();
        task.setUserId(user.getId());
        task.setId(0L);
        task.setName("tn");
        task.setDescription("td");

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            doReturn(task).when(taskService).findById(1L,true);
            taskListener.setTaskUserById(user);
        });
    }

    @Test
    void setTaskUserByIndex() {
        User user = new User("user");
        user.setRole(Role.USER);
        taskListener.setUser(user);
        user.setRole(Role.ADMIN);

        Task task = new Task();
        task.setUserId(user.getId());
        task.setId(0L);
        task.setName("tn");
        task.setDescription("td");

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,true);
            taskListener.setTaskUserByIndex(user);
        });

    }


}