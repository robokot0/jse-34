package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.publisher.InputConsole;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserListenerTest {
    static UserListener userListener;
    static UserService userService;
    static ProjectListener projectListener;
    static TaskListener taskListener;
    static InputConsole inputConsole;
    static CommandHistoryService commandHistoryService;

    @BeforeAll
    public static void doBeforeAll() {
        commandHistoryService = Mockito.mock(CommandHistoryService.class);
        userService = Mockito.mock(UserService.class);
        projectListener = Mockito.mock(ProjectListener.class);
        taskListener =  Mockito.mock(TaskListener.class);
        inputConsole =  Mockito.mock(InputConsole.class);
        userListener = new UserListener(projectListener,taskListener,userService,commandHistoryService);
        userListener.setConsole(inputConsole);
    }

    @Test
    void setUsergetUser() {
        User user = new User("login");
        userListener.setUser(user);
        assertEquals(user,userListener.getUser());
    }

    @Test
    void enterStringCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputString("pn",false);
            userListener.enterStringCommandParameter("pn");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn","pv");
        });
    }

    @Test
    void enterPasswordCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputPassword("pn1",false);
            userListener.enterPasswordCommandParameter("pn1");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn1","******");
        });
    }

    @Test
    void enterIntegerCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn2",true);
            assertEquals(1,userListener.enterIntegerCommandParameter("pn2"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn2","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn3",true);
            assertEquals(1,userListener.enterIntegerCommandParameter("pn3"));
        });
    }

    @Test
    void enterLongCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn4",true);
            assertEquals(1L,userListener.enterLongCommandParameter("pn4"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn4","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn5",true);
            assertEquals(1L,userListener.enterLongCommandParameter("pn5"));
        });
    }

    @Test
    void showResult() {
        assertDoesNotThrow(()-> {
            userListener.ShowResult("result");
            verify(inputConsole,times(1)).showMessage("result");
            verify(commandHistoryService,times(1)).AddCommandResultToLastCommand("result");
        });
    }

    @Test
    void testAuthUser() {
        userListener.setUser(new User("testAuthUser"));
        assertTrue(userListener.testAuthUser());
        userListener.setUser(null);
        assertFalse(userListener.testAuthUser());
    }

    @Test
    void testAdminUser() {
        User user = new User("testAdminUser");
        user.setRole(Role.ADMIN);
        userListener.setUser(user);
        assertTrue(userListener.testAdminUser());
        user.setRole(Role.USER);
        assertFalse(userListener.testAdminUser());
        userListener.setUser(null);
        assertFalse(userListener.testAdminUser());
    }

    @Test
    void getInstance() {
        UserListener userListener = UserListener.getInstance();
        assertEquals(userListener,UserListener.getInstance());
    }

    @Test
    void testNotify() {
        assertEquals(-1,userListener.notify("aaaaaa"));
        assertEquals(0,userListener.notify(SHORT_USER_CREATE));
        assertEquals(0,userListener.notify(USER_CREATE));
        assertEquals(0,userListener.notify(SHORT_USER_LIST));
        assertEquals(0,userListener.notify(USER_LIST));
        assertEquals(0,userListener.notify(SHORT_USER_VIEW_BY_ID));
        assertEquals(0,userListener.notify(USER_VIEW_BY_ID));
        assertEquals(0,userListener.notify(SHORT_USER_VIEW_BY_INDEX));
        assertEquals(0,userListener.notify(USER_VIEW_BY_INDEX));
        assertEquals(0,userListener.notify(SHORT_USER_VIEW_BY_LOGIN));
        assertEquals(0,userListener.notify(USER_VIEW_BY_LOGIN));
        assertEquals(0,userListener.notify(SHORT_USER_REMOVE_BY_ID));
        assertEquals(0,userListener.notify(USER_REMOVE_BY_ID));
        assertEquals(0,userListener.notify(SHORT_USER_REMOVE_BY_INDEX));
        assertEquals(0,userListener.notify(USER_REMOVE_BY_INDEX));
        assertEquals(0,userListener.notify(SHORT_USER_REMOVE_BY_LOGIN));
        assertEquals(0,userListener.notify(USER_REMOVE_BY_LOGIN));
        assertEquals(0,userListener.notify(SHORT_USER_UPDATE_BY_ID));
        assertEquals(0,userListener.notify(USER_UPDATE_BY_ID));
        assertEquals(0,userListener.notify(SHORT_USER_UPDATE_BY_INDEX));
        assertEquals(0,userListener.notify(USER_UPDATE_BY_INDEX));
        assertEquals(0,userListener.notify(SHORT_USER_UPDATE_BY_LOGIN));
        assertEquals(0,userListener.notify(USER_UPDATE_BY_LOGIN));
        assertEquals(0,userListener.notify(SHORT_USER_UPDATE_PASSWORD_BY_ID));
        assertEquals(0,userListener.notify(USER_UPDATE_PASSWORD_BY_ID));
        assertEquals(0,userListener.notify(SHORT_USER_UPDATE_PASSWORD_BY_INDEX));
        assertEquals(0,userListener.notify(USER_UPDATE_PASSWORD_BY_INDEX));
        assertEquals(0,userListener.notify(SHORT_USER_UPDATE_PASSWORD_BY_LOGIN));
        assertEquals(0,userListener.notify(USER_UPDATE_PASSWORD_BY_LOGIN));
        assertEquals(0,userListener.notify(SHORT_USER_AUTH));
        assertEquals(0,userListener.notify(USER_AUTH));
        assertEquals(0,userListener.notify(SHORT_USER_UPDATE_PASSWORD));
        assertEquals(0,userListener.notify(USER_UPDATE_PASSWORD));
        assertEquals(0,userListener.notify(SHORT_USER_VIEW));
        assertEquals(0,userListener.notify(USER_VIEW));
        assertEquals(0,userListener.notify(SHORT_USER_UPDATE));
        assertEquals(0,userListener.notify(USER_UPDATE));
        assertEquals(0,userListener.notify(SHORT_USER_END));
        assertEquals(0,userListener.notify(USER_END));
        assertEquals(0,userListener.notify(SHORT_USER_OF_TASK_SET_BY_INDEX));
        assertEquals(0,userListener.notify(USER_OF_TASK_SET_BY_INDEX));
        assertEquals(0,userListener.notify(SHORT_USER_OF_PROJECT_SET_BY_INDEX));
        assertEquals(0,userListener.notify(USER_OF_PROJECT_SET_BY_INDEX));
        assertEquals(0,userListener.notify(SHORT_SAVE_TO_JSON));
        assertEquals(0,userListener.notify(SAVE_TO_JSON));
        assertEquals(0,userListener.notify(SHORT_SAVE_TO_XML));
        assertEquals(0,userListener.notify(SAVE_TO_XML));
        assertEquals(0,userListener.notify(SHORT_LOAD_FROM_XML));
        assertEquals(0,userListener.notify(LOAD_FROM_XML));
        assertEquals(0,userListener.notify(SHORT_LOAD_FROM_JSON));
        assertEquals(0,userListener.notify(LOAD_FROM_JSON));
        assertEquals(0,userListener.notify(SHORT_HELP));
        assertEquals(0,userListener.notify(HELP));

        assertDoesNotThrow(()-> {
            Mockito.doThrow(IOException.class).when(userService).loadFrom(any(),any());
            userListener.notify(LOAD_FROM_JSON);
        });
    }

    @Test
    void authUser() {
        User user = new User("login");
        user.setPasswordHash("password");


        assertDoesNotThrow(()-> {
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            doReturn(null).when(userService).findByLogin("login");
            assertEquals(0,userListener.authUser());

            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            doReturn(user).when(userService).findByLogin("login");
            doReturn("password").when(inputConsole).getNextInputPassword("user password",false);
            doReturn("password1").when(userService).getStringHash("password");
            assertEquals(0,userListener.authUser());

            doReturn("password").when(userService).getStringHash("password");
            assertEquals(0,userListener.authUser());
        });
    }

    @Test
    void updateAuthUserPassword() {
        assertDoesNotThrow(()-> {
            userListener.updateAuthUserPassword();
        });
    }

    @Test
    void viewAuthUser() {
        assertDoesNotThrow(()-> {
            userListener.viewAuthUser();
        });
    }

    @Test
    void updateAuthUser() {
        assertDoesNotThrow(()-> {
            userListener.updateAuthUser();
        });
    }

    @Test
    void endAuthUserSession() {
        assertDoesNotThrow(()-> {
            userListener.endAuthUserSession();
        });
    }

    @Test
    void updateUserDataNoRole() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);

        assertDoesNotThrow(()-> {
            doReturn("ul").when(inputConsole).getNextInputString("user login",false);
            doReturn("fn").when(inputConsole).getNextInputString("user first name",false);
            doReturn("sn").when(inputConsole).getNextInputString("user second name",false);
            doReturn("mn").when(inputConsole).getNextInputString("user middle name",false);
            userListener.updateUserDataNoRole(user);
            verify(userService).updateData(user.getId(),"ul",user.getRole(),"fn","sn","mn");
        });
    }

    @Test
    void updateUserData() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);

        assertDoesNotThrow(()-> {
            doReturn("ul").when(inputConsole).getNextInputString("user login",false);
            doReturn("ADMIN").when(inputConsole).getNextInputString("user role",false);
            doReturn("fn").when(inputConsole).getNextInputString("user first name",false);
            doReturn("sn").when(inputConsole).getNextInputString("user second name",false);
            doReturn("mn").when(inputConsole).getNextInputString("user middle name",false);
            userListener.updateUserData(user);
            verify(userService).updateData(user.getId(),"ul",Role.ADMIN,"fn","sn","mn");
            userListener.updateUserData(null);
        });
    }

    @Test
    void updateUserPassword() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);

        assertDoesNotThrow(()-> {
            doReturn("password").when(inputConsole).getNextInputPassword("user password",false);
            doReturn("password1").when(inputConsole).getNextInputPassword("user password confirmation",false);
            userListener.updateUserPassword(user);
            doReturn("password").when(inputConsole).getNextInputPassword("user password confirmation",false);
            doReturn("password").when(userService).getStringHash("password");
            userListener.updateUserPassword(user);
            userListener.updateUserPassword(null);
        });
    }

    @Test
    void updateUserDataByLogin() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.updateUserDataByLogin());
            user.setRole(Role.ADMIN);
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            assertEquals(0,userListener.updateUserDataByLogin());
            doReturn(user).when(userService).findByLogin("login");
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            doReturn("ADMIN").when(inputConsole).getNextInputString("user role",false);
            doReturn("fn").when(inputConsole).getNextInputString("user first name",false);
            doReturn("sn").when(inputConsole).getNextInputString("user second name",false);
            doReturn("mn").when(inputConsole).getNextInputString("user middle name",false);
            assertEquals(0,userListener.updateUserDataByLogin());
        });
    }

    @Test
    void updateUserPasswordByLogin() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.updateUserPasswordByLogin());
            user.setRole(Role.ADMIN);
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            assertEquals(0,userListener.updateUserPasswordByLogin());
            doReturn(user).when(userService).findByLogin("login");
            doReturn("password").when(inputConsole).getNextInputPassword("user password",false);
            doReturn("password").when(inputConsole).getNextInputPassword("user password confirmation",false);
            doReturn("password").when(userService).getStringHash("password");
            assertEquals(0,userListener.updateUserPasswordByLogin());
        });
    }

    @Test
    void updateUserDataById() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(1L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("user ID",true);
            assertEquals(0,userListener.updateUserDataById());
            doReturn(user).when(userService).findById(1L);
            doReturn("ul").when(inputConsole).getNextInputString("user login",false);
            doReturn("ADMIN").when(inputConsole).getNextInputString("user role",false);
            doReturn("fn").when(inputConsole).getNextInputString("user first name",false);
            doReturn("sn").when(inputConsole).getNextInputString("user second name",false);
            doReturn("mn").when(inputConsole).getNextInputString("user middle name",false);
            assertEquals(0,userListener.updateUserDataById());
        });
    }

    @Test
    void updateUserPasswordById() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(1L);
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("user ID",true);
            doReturn(null).when(userService).findById(1L);
            assertEquals(0,userListener.updateUserPasswordById());
            doReturn(user).when(userService).findById(1L);
            doReturn("password").when(inputConsole).getNextInputPassword("user password",false);
            doReturn("password").when(inputConsole).getNextInputPassword("user password confirmation",false);
            doReturn("password").when(userService).getStringHash("password");
            assertEquals(0,userListener.updateUserPasswordById());
        });
    }

    @Test
    void updateUserDataByIndex() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(1L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.updateUserDataByIndex());
            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("user index",true);
            doReturn(null).when(userService).findByIndex(0);
            assertEquals(0,userListener.updateUserDataByIndex());
            doReturn(user).when(userService).findByIndex(0);
            doReturn("ul").when(inputConsole).getNextInputString("user login",false);
            doReturn("ADMIN").when(inputConsole).getNextInputString("user role",false);
            doReturn("fn").when(inputConsole).getNextInputString("user first name",false);
            doReturn("sn").when(inputConsole).getNextInputString("user second name",false);
            doReturn("mn").when(inputConsole).getNextInputString("user middle name",false);
            assertEquals(0,userListener.updateUserDataByIndex());
        });
    }

    @Test
    void updateUserPasswordByIndex() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(1L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.updateUserPasswordByIndex());
            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("user index",true);
            assertEquals(0,userListener.updateUserPasswordByIndex());
            doReturn(user).when(userService).findByIndex(0);
            doReturn("password").when(inputConsole).getNextInputPassword("user password",false);
            doReturn("password").when(inputConsole).getNextInputPassword("user password confirmation",false);
            doReturn("password").when(userService).getStringHash("password");
            assertEquals(0,userListener.updateUserPasswordByIndex());
        });
    }

    @Test
    void removeUserByLogin() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(1L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.removeUserByLogin());
            user.setRole(Role.ADMIN);
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            doReturn(null).when(userService).findByLogin("login");
            assertEquals(0,userListener.removeUserByLogin());
            doReturn(user).when(userService).findByLogin("login");
            assertEquals(0,userListener.removeUserByLogin());
            doReturn(user).when(userService).removeById(user.getId());
            assertEquals(0,userListener.removeUserByLogin());
        });
    }

    @Test
    void removeUserById() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.removeUserById());
            user.setRole(Role.ADMIN);
            doReturn("0").when(inputConsole).getNextInputString("user ID",true);
            doReturn(null).when(userService).removeById(0L);
            assertEquals(0,userListener.removeUserById());
            doReturn(user).when(userService).removeById(0L);
            assertEquals(0,userListener.removeUserById());
        });
    }

    @Test
    void removeUserByIndex() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.removeUserByIndex());
            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("user index",true);
            doReturn(null).when(userService).findByIndex(0);
            doReturn(null).when(userService).removeById(0L);
            assertEquals(0,userListener.removeUserByIndex());

            doReturn(user).when(userService).findByIndex(0);
            doReturn(null).when(userService).removeById(0L);
            assertEquals(0,userListener.removeUserByIndex());

            doReturn(user).when(userService).findByIndex(0);
            doReturn(user).when(userService).removeById(0L);
            assertEquals(0,userListener.removeUserByIndex());
        });
    }

    @Test
    void createUser() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.createUser());

            user.setRole(Role.ADMIN);
            doReturn("ul").when(inputConsole).getNextInputString("user login",false);
            doReturn("ADMIN").when(inputConsole).getNextInputString("user role",false);
            doReturn("fn").when(inputConsole).getNextInputString("user first name",false);
            doReturn("sn").when(inputConsole).getNextInputString("user second name",false);
            doReturn("mn").when(inputConsole).getNextInputString("user middle name",false);
            doReturn("password").when(inputConsole).getNextInputPassword("user password",false);
            doReturn("password1").when(inputConsole).getNextInputPassword("user password confirmation",false);
            assertEquals(0,userListener.createUser());

            user.setRole(Role.ADMIN);
            doReturn("password").when(inputConsole).getNextInputPassword("user password confirmation",false);
            doReturn("password").when(userService).getStringHash("password");
            assertEquals(0,userListener.createUser());
        });
    }

    @Test
    void viewUserByLogin() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(1L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.viewUserByLogin());

            user.setRole(Role.ADMIN);
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            doReturn(user).when(userService).findByLogin("login");
            assertEquals(0,userListener.viewUserByLogin());
        });
    }

    @Test
    void viewUserById() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.viewUserById());

            user.setRole(Role.ADMIN);
            doReturn("0").when(inputConsole).getNextInputString("user ID",true);
            doReturn(user).when(userService).removeById(0L);
            assertEquals(0,userListener.viewUserById());
        });
    }

    @Test
    void viewUserByIndex() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);
        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.viewUserByIndex());

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("user index",true);
            doReturn(user).when(userService).findByIndex(0);
            assertEquals(0,userListener.viewUserByIndex());
        });
    }

    @Test
    void listUser() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);

        List<User> users = new ArrayList<>();
        users.add(user);

        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.listUser());

            user.setRole(Role.ADMIN);
            doReturn(users).when(userService).findAll();
            assertEquals(0,userListener.listUser());
        });
    }

    @Test
    void setProjectUserById() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);

        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.setProjectUserById());

            user.setRole(Role.ADMIN);
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            doReturn(user).when(userService).findByLogin("login");
            assertEquals(0,userListener.setProjectUserById());
        });
    }

    @Test
    void setProjectUserByIndex() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);

        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.setProjectUserByIndex());

            user.setRole(Role.ADMIN);
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            doReturn(user).when(userService).findByLogin("login");
            assertEquals(0,userListener.setProjectUserByIndex());
        });
    }

    @Test
    void setTaskUserById() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);

        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.setTaskUserById());

            user.setRole(Role.ADMIN);
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            doReturn(user).when(userService).findByLogin("login");
            assertEquals(0,userListener.setTaskUserById());
        });
    }

    @Test
    void setTaskUserByIndex() {
        User user = new User("login");
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);

        assertDoesNotThrow(()-> {
            assertEquals(0,userListener.setTaskUserByIndex());

            user.setRole(Role.ADMIN);
            doReturn("login").when(inputConsole).getNextInputString("user login",false);
            doReturn(user).when(userService).findByLogin("login");
            assertEquals(0,userListener.setTaskUserByIndex());
        });
    }

}