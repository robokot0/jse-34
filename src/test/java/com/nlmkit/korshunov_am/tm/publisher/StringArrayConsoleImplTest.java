package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringArrayConsoleImplTest {
    @Test
    void getNextInputString() {
        StringArrayConsoleImpl stringArrayConsole = new StringArrayConsoleImpl(new String[]{"String1", "String2"});
        assertDoesNotThrow(()->{
            assertEquals("String1",stringArrayConsole.getNextInputString("pn1",true));
            assertEquals("String2",stringArrayConsole.getNextInputString("pn2",false));
            stringArrayConsole.getNextInputString("pn3",false);
        });
        assertThrows(WrongArgumentException.class,()-> stringArrayConsole.getNextInputString("pn3",true));
    }

    @Test
    void getNextInputInteger() {
        StringArrayConsoleImpl stringArrayConsole = new StringArrayConsoleImpl(new String[]{"1", "2","aa"});
        assertDoesNotThrow(()->{
            assertEquals(1,stringArrayConsole.getNextInputInteger("pn1",true));
            assertEquals(2,stringArrayConsole.getNextInputInteger("pn2",false));
        });
        assertThrows(WrongArgumentException.class,()-> stringArrayConsole.getNextInputInteger("pn3",true));
        assertDoesNotThrow(()->{
            stringArrayConsole.getNextInputInteger("pn3",false);
        });
        assertThrows(WrongArgumentException.class,()-> stringArrayConsole.getNextInputInteger("pn3",true));
    }

    @Test
    void getNextInputLong() {
        StringArrayConsoleImpl stringArrayConsole = new StringArrayConsoleImpl(new String[]{"1", "2","aa"});
        assertDoesNotThrow(()->{
            assertEquals(1,stringArrayConsole.getNextInputLong("pn1",true));
            assertEquals(2,stringArrayConsole.getNextInputLong("pn2",false));
        });
        assertThrows(WrongArgumentException.class,()-> stringArrayConsole.getNextInputLong("pn3",true));
        assertDoesNotThrow(()->{
            stringArrayConsole.getNextInputLong("pn3",false);
        });
        assertThrows(WrongArgumentException.class,()-> stringArrayConsole.getNextInputLong("pn3",true));
    }

    @Test
    void getNextInputPassword() {
        StringArrayConsoleImpl stringArrayConsole = new StringArrayConsoleImpl(new String[]{"String1", "String2"});
        assertDoesNotThrow(()->{
            assertEquals("String1",stringArrayConsole.getNextInputPassword("pn1",true));
            assertEquals("String2",stringArrayConsole.getNextInputPassword("pn2",false));
            stringArrayConsole.getNextInputPassword("pn3",false);
        });
        assertThrows(WrongArgumentException.class,()-> stringArrayConsole.getNextInputPassword("pn3",true));
    }

    @Test
    void showMessage() {
        StringArrayConsoleImpl stringArrayConsole = new StringArrayConsoleImpl(new String[]{"1", "2","aa"});
        assertDoesNotThrow(()-> stringArrayConsole.showMessage("message"));
    }
}