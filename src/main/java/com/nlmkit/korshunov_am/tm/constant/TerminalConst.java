package com.nlmkit.korshunov_am.tm.constant;

public class TerminalConst {
    /**
     * Системные комманды
     */
    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String EXIT = "exit";
    public static final String COMMAND_HISTORY_VIEW = "command-history-view";
    public static final String SAVE_TO_JSON = "save-to-json";
    public static final String SAVE_TO_XML = "save-to-xml";
    public static final String LOAD_TEST_DATA = "load-test-data";
    public static final String LOAD_FROM_JSON = "load-from-json";
    public static final String LOAD_FROM_XML = "load-from-xml";
    /**
     * Комманды проектов
     */
    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW = "project-view";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    /**
     * Комманды задач
     */
    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW = "task-view";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_LISTS_BY_PROJECT_ID = "task-list-by-project-id";
    public static final String TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-by-ids";
    public static final String TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-from-project-by-ids";
    /**
     * Комманды пользователей
     */
    public static final String USER_CREATE = "user-create";
    public static final String USER_LIST = "user-list";
    public static final String USER_VIEW_BY_LOGIN = "user-view-by-login";
    public static final String USER_VIEW_BY_ID = "user-view-by-id";
    public static final String USER_VIEW_BY_INDEX = "user-view-by-index";
    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    public static final String USER_REMOVE_BY_ID = "user-remove-by-id";
    public static final String USER_REMOVE_BY_INDEX = "user-remove-by-index";
    public static final String USER_UPDATE_BY_LOGIN = "user-update-by-login";
    public static final String USER_UPDATE_BY_ID = "user-update-by-id";
    public static final String USER_UPDATE_BY_INDEX = "user-update-by-index";
    public static final String USER_UPDATE_PASSWORD_BY_LOGIN = "user-update-password-by-login";
    public static final String USER_UPDATE_PASSWORD_BY_ID = "user-update-password-by-id";
    public static final String USER_UPDATE_PASSWORD_BY_INDEX = "user-update-password-by-index";
    public static final String USER_AUTH = "user-auth";
    public static final String USER_UPDATE_PASSWORD = "user-update-password";
    public static final String USER_VIEW = "user-view";
    public static final String USER_UPDATE = "user-update";
    public static final String USER_END = "user-end-session";
    public static final String USER_OF_PROJECT_SET_BY_INDEX = "user-of-project-set-by-index";
    public static final String USER_OF_TASK_SET_BY_INDEX = "user-of-task-set-by-index";
    /**
     * Сокращенные системные комманды
     */
    public static final String SHORT_HELP = "h";
    public static final String SHORT_VERSION = "v";
    public static final String SHORT_ABOUT = "a";
    public static final String SHORT_EXIT = "e";
    public static final String SHORT_COMMAND_HISTORY_VIEW = "chv";
    public static final String SHORT_SAVE_TO_JSON = "sj";
    public static final String SHORT_SAVE_TO_XML = "sx";
    public static final String SHORT_LOAD_TEST_DATA = "ltd";
    public static final String SHORT_LOAD_FROM_JSON = "lj";
    public static final String SHORT_LOAD_FROM_XML = "lx";
    /**
     * Сокращенные комманды проектов
     */
    public static final String SHORT_PROJECT_CREATE = "pcr";
    public static final String SHORT_PROJECT_CLEAR = "pcl";
    public static final String SHORT_PROJECT_LIST = "pl";
    public static final String SHORT_PROJECT_VIEW = "pv";
    public static final String SHORT_PROJECT_REMOVE_BY_ID = "prid";
    public static final String SHORT_PROJECT_REMOVE_BY_NAME = "prn";
    public static final String SHORT_PROJECT_REMOVE_BY_INDEX = "prin";
    public static final String SHORT_PROJECT_UPDATE_BY_INDEX = "puin";
    /**
     * Сокращенные комманды задач
     */
    public static final String SHORT_TASK_CREATE = "tcr";
    public static final String SHORT_TASK_CLEAR = "tcl";
    public static final String SHORT_TASK_LIST = "tl";
    public static final String SHORT_TASK_VIEW = "tv";
    public static final String SHORT_TASK_REMOVE_BY_ID = "trid";
    public static final String SHORT_TASK_REMOVE_BY_NAME = "trn";
    public static final String SHORT_TASK_REMOVE_BY_INDEX = "trin";
    public static final String SHORT_TASK_UPDATE_BY_INDEX = "tuin";
    public static final String SHORT_TASK_LISTS_BY_PROJECT_ID = "tlp";
    public static final String SHORT_TASK_ADD_TO_PROJECT_BY_IDS = "tap";
    public static final String SHORT_TASK_REMOVE_FROM_PROJECT_BY_IDS = "trp";
    /**
     * Сокращенные комманды пользователей
     */
    public static final String SHORT_USER_CREATE = "uc";
    public static final String SHORT_USER_LIST = "ul";
    public static final String SHORT_USER_VIEW_BY_LOGIN = "uvl";
    public static final String SHORT_USER_VIEW_BY_ID = "uvid";
    public static final String SHORT_USER_VIEW_BY_INDEX = "uvi";
    public static final String SHORT_USER_REMOVE_BY_LOGIN = "url";
    public static final String SHORT_USER_REMOVE_BY_ID = "urid";
    public static final String SHORT_USER_REMOVE_BY_INDEX = "uri";
    public static final String SHORT_USER_UPDATE_BY_LOGIN = "uul";
    public static final String SHORT_USER_UPDATE_BY_ID = "uuid";
    public static final String SHORT_USER_UPDATE_BY_INDEX = "uui";
    public static final String SHORT_USER_UPDATE_PASSWORD_BY_LOGIN = "uupl";
    public static final String SHORT_USER_UPDATE_PASSWORD_BY_ID = "uupid";
    public static final String SHORT_USER_UPDATE_PASSWORD_BY_INDEX = "uupi";
    public static final String SHORT_USER_AUTH = "ua";
    public static final String SHORT_USER_UPDATE_PASSWORD = "uup";
    public static final String SHORT_USER_VIEW = "uv";
    public static final String SHORT_USER_UPDATE = "uu";
    public static final String SHORT_USER_END = "ue";
    public static final String SHORT_USER_OF_PROJECT_SET_BY_INDEX = "up";
    public static final String SHORT_USER_OF_TASK_SET_BY_INDEX = "ut";
}
