package com.nlmkit.korshunov_am.tm;

import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.listener.*;
import com.nlmkit.korshunov_am.tm.publisher.*;

/**
 *  Тестовое приложение
 */
public class App {
    /**
     * Источник комманд
     */
    static Publisher publisher = new PublisherImpl(ScannerConsoleImpl.getInstance())
            .addListener(SystemListener.getInstance())
            .addListener(CommandHistoryListener.getInstance())
            .addListener(ProjectListener.getInstance())
            .addListener(TaskListener.getInstance())
            .addListener(UserListener.getInstance());
    /**
     * Точка входа
     * @param args Аргументы коммандной строки.
     */
    public static void main(String[] args) throws WrongArgumentException {
        if (args != null)
            if (args.length >=1) {
                publisher.setConsole(new StringArrayConsoleImpl(args));
                publisher.readAndExecuteCommand();
                publisher.setConsole(ScannerConsoleImpl.getInstance());
            }
        publisher.readAndExecuteCommand();
    }

}
