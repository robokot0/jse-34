package com.nlmkit.korshunov_am.tm.entity;

public interface EntityId {
    /**
     * Получить идентификатор
     * @return идентификатор
     */
    Long getId();

    /**
     * Задать идентификатор
     * @param id идентификатор
     */
    void setId(final Long id);
}
