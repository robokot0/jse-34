package com.nlmkit.korshunov_am.tm.entity;

public class Command {
    /**
     * Комманда
     */
    private String commandText;

    /**
     * Получить текст комманды
     * @return текст комманды
     */
    public String getCommandText() {
        return commandText;
    }

    /**
     * Задать текст комманды
     * @param commandText текст комманды
     */
    public void setCommandText(String commandText) {
        this.commandText = commandText;
    }

    /**
     * Конструктор по умолчанию
     * @param commandText Текст комманды
     */
    public Command(String commandText) {
        this.commandText = commandText;
    }

    public void addCommandText(String commandText){
        this.commandText=(this.commandText+" "+commandText).trim();
    }

}
