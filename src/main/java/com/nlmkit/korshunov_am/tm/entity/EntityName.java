package com.nlmkit.korshunov_am.tm.entity;

public interface EntityName {
    /**
     * Получить имя
     * @return имя
     */
    String getName();

    /**
     * ЗАдать имя
     * @param name имя
     */
    void setName(final String name);
}
