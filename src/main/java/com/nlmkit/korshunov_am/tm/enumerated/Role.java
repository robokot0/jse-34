package com.nlmkit.korshunov_am.tm.enumerated;

public enum Role {
    USER( "User"),
    ADMIN( "Administrator");
    /**
     * Имя для отображения
     */
    private final String displayName;
    /**
     * Конструктор
     * @param displayName имя для отображения
     */
    Role(String displayName) {
        this.displayName = displayName;
    }
    /**
     * Переопределение toString перевода в строку для отображения стандартными методами
     * @return имя для отображения
     */
    @Override
    public String toString() {
        return displayName;
    }
}
