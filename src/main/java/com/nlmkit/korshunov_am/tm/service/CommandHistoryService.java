package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Command;
import com.nlmkit.korshunov_am.tm.repository.CommandHistoryRepository;

import java.util.List;

public class CommandHistoryService extends AbstractService {
    /**
     * Ссылка на репозиторий истории комманд
     */
    private final CommandHistoryRepository commandHistoryRepository;
    /**
     * конструктор по умолчанию
     */
    private CommandHistoryService(){
        super();
        this.commandHistoryRepository =  CommandHistoryRepository.getInstance();
    }
    /**
     * конструктор для тестирования
     */
    public CommandHistoryService(CommandHistoryRepository commandHistoryRepository){
        super();
        this.commandHistoryRepository = commandHistoryRepository;
    }
    /**
     * Единственный экземпляр объекта CommandHistoryService
     */
    private static CommandHistoryService instance = null;

    /**
     * Получить единственный экземпляр объекта CommandHistoryService
     * @return единственный экземпляр объекта CommandHistoryService
     */
    public static CommandHistoryService getInstance(){
        if (instance == null){
            instance = new CommandHistoryService();
        }
        return instance;
    }


    /**
     * Добавляет комманду в историю
     * @param command комманда
     */
    public void addCommandToHistory(String command) {
        commandHistoryRepository.AddCommandToHistory(command);
    }

    /**
     * Добавляет параметр комманды в историю
     * @param commandParameter параметр комманды
     * @param parameterValue значение параметра
     */
    public void AddCommandParameterToLastCommand(String commandParameter, String parameterValue) {
        commandHistoryRepository.AddCommandParameterToLastCommand(commandParameter, parameterValue);
    }

    /**
     * Добавляет результат исполнения комманды
     * @param commandResult результат исполнения комманды
     */
    public void AddCommandResultToLastCommand(String commandResult) {
        commandHistoryRepository.AddCommandResultToLastCommand(commandResult);
    }

    /**
     * Получить список всех комманд из истории
     * @return список комманд
     */
    public List<Command> findAll() {
        return commandHistoryRepository.findAll();
    }



}
