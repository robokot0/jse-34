package com.nlmkit.korshunov_am.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.repository.UserRepository;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class UserService extends AbstractService{
    private final java.security.MessageDigest md;
    /**
     * Приватный конструктор по умолчанию
     */
    private UserService() {
        super();
        this.userRepository = UserRepository.getInstance();
        this.md = getMd("MD5");
    }
    /**
     * конструктор для тестирования
     */
    public UserService(UserRepository userRepository,java.security.MessageDigest md){
        super();
        this.userRepository=userRepository;
        this.md=md;
    }
    /**
     * Возвращает алгоритм по названию
     * @param algoritm algoritm
     * @return MessageDigest
     */
    public static java.security.MessageDigest getMd(String algoritm) {
        java.security.MessageDigest md;
        try {
            md = java.security.MessageDigest.getInstance(algoritm);
        } catch (NoSuchAlgorithmException e) {
            md=null;
        }
        return md;
    }
    /**
     * Единственный экземпляр объекта UserService
     */
    private static UserService instance = null;
    /**
     * Получить единственный экземпляр объекта UserService
     * @return единственный экземпляр объекта UserService
     */
    public static UserService getInstance() {
        if (instance == null){
            instance = new UserService();
        }
        return instance;
    }
    /**
     * Репозитарий пользователей
     */
    private final UserRepository userRepository;

    /**
     * Вычислить хэш строки
     * @param stringtohash строка для хэширования
     * @return хэш строки
     */
    public String getStringHash(String stringtohash) {
        if (this.md==null) {
            return null;
        } else {
            byte[] array = md.digest(stringtohash.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        }
    }
    /**
     * Создать пользователя
     * @param login имя
     * @return пользователь
     */
    public User create(String login) {
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findByLogin(login)!=null) return null;
        return userRepository.create(login);
    }

    /**
     * Создать пользователя
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User create(final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findByLogin(login)!=null) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.create(login,role,firstName,secondName,middleName,passwordHash);
    }
    /**
     * Изменить пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User update(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        if (id == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        if (userRepository.findById(id)==null) return null;
        if (!userRepository.findByLogin(login).getId().equals(id)) return null;
        return userRepository.update(id,login,role,firstName,secondName,middleName,passwordHash);
    }
    /**
     * Изменить данные пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @return пользователь
     */
    public User updateData(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName) {
        if (id == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findById(id)==null) return null;
        if (!(userRepository.findByLogin(login)==null))
          if (!userRepository.findByLogin(login).getId().equals(id)) return null;
        return userRepository.updateData(id,login,role,firstName,secondName,middleName);
    }
    /**
     * Изменить пароль пользователя
     * @param id идентификатор
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User updatePassword(final Long id, String passwordHash) {
        if (id == null) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        if (userRepository.findById(id)==null) return null;
        return userRepository.updatePasswordHash(id, passwordHash);
    }

    /**
     * Удалить всех пользователей.
     */
    public void clear() {
        userRepository.clear();
    }
    /**
     * Найти пользователя по id.
     * @param id id
     * @return пользователь
     */
    public User findById(final Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }
    /**
     * Найти пользователя по login.
     * @param login login
     * @return пользователь
     */
    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }
    /**
     * Найти пользователя по индексу.
     * @param index index
     * @return пользователь
     */
    public User findByIndex(final int index) {
        return userRepository.findByIndex(index);
    }
    /**
     * Удалить пользователя по id
     * @param id id
     * @return login
     */
    public User removeById(final Long id) {
        if (id == null) return null;
        if (userRepository.findById(id)==null) return null;
        return userRepository.removeById(id);
    }
    /**
     * Получить список всех пользователей
     * @return список пользователей
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Сохранить в поток
     * @throws IOException ошибка ввода вывода
     * @param objectMapper objectMapper
     * @param outputStream outputStream
     */
    public void saveAs(ObjectMapper objectMapper, OutputStream outputStream) throws IOException {
        userRepository.saveAs(objectMapper, outputStream);
    }

    /**
     * Загрузить из потока
     * @throws IOException ошибка ввода вывода
     * @param objectMapper objectMapper
     * @param inputStream inputStream
     */
    public void loadFrom(ObjectMapper objectMapper, InputStream inputStream) throws IOException {
        userRepository.loadFrom(objectMapper, inputStream);
    }
}
