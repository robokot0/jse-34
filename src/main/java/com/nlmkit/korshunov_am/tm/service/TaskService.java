package com.nlmkit.korshunov_am.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Сервис задач
 */
public class TaskService extends AbstractService{
    /**
     * Приватный конструктор по умолчанию
     */
    private TaskService(){
        super();
        this.taskRepostory = TaskRepostory.getInstance();
    }
    /**
     * конструктор для тестирования
     */
    public TaskService(TaskRepostory taskRepostory){
        super();
        this.taskRepostory = taskRepostory;
    }
    /**
     * Единственный экземпляр объекта TaskService
     */
    private static TaskService instance = null;

    /**
     * Получить единственный экземпляр объекта TaskService
     * @return единственный экземпляр объекта TaskService
     */
    public static TaskService getInstance(){
        if (instance == null){
            instance = new TaskService();
        }
        return instance;
    }



    /**
     * Репозитарий задач
     */
    private final TaskRepostory taskRepostory;


    /**
     * Создать задачу
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name,final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        return taskRepostory.create(name,userId);
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name, final String description,final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание задачи");
        return taskRepostory.create(name, description, userId);
    }

    /**
     * Измениь задачу
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task update(final Long id, final String name, final String description,final Long userId) throws WrongArgumentException{
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание задачи");
        return taskRepostory.update(id, name, description, userId);
    }

    /**
     * Удалить все задачи
     */
    public void clear() {
        taskRepostory.clear();
    }

    /**
     * Удалить все задачи пользователя
     */
    public void clear(final Long userId ) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        taskRepostory.clear(userId);
    }

    /**
     * Найти по индексу
     * @param index индекс
     * @return задача
     */
    public Task findByIndex(final Integer index,final boolean throwIfNotFound) throws WrongArgumentException, TaskNotFoundException {
        if (index == null) {
            throw new WrongArgumentException("Не задан индекс");
        }
        if (index < 0) {
            throw new WrongArgumentException("Индекс должен быть больше нуля");
        }
        final Task task = taskRepostory.findByIndex(index);
        if(task==null && throwIfNotFound) {
            throw new TaskNotFoundException("индекс", index.toString());
        }
        return task;
    }

    /**
     * мНайти по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByIndex(final Integer index,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Task task = taskRepostory.findByIndex(index,userId);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("индекс",userId,index.toString());
        return task;
    }

    /**
     * Найти по имени
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name,final boolean throwIfNotFound) throws WrongArgumentException, TaskNotFoundException{
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.findByName(name);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("имя",name);
        return task;
    }

    /**
     * Найти по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByName(final String name,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.findByName(name);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("имя",userId,name);
        return task;
    }

    /**
     * Найти по идентификатору
     * @param id идентификатор
     * @return задча
     */
    public Task findById(final Long id,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findById(id);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("ИД",id.toString());
        return task;
    }

    /**
     * Найти по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задча
     */
    public Task findById(final Long id,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException  {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findById(id,userId);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("ИД",userId,id.toString());
        return task;
    }

    /**
     * Удалить по индексу
     * @param index индекс
     * @return задача
     */
    public Task removeByIndex(final Integer index) throws WrongArgumentException, TaskNotFoundException {
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.removeByIndex(index);
        if(task==null) throw new TaskNotFoundException("индекс",index.toString());
        return task;
    }

    /**
     * Удалить по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByIndex(final Integer index,final Long userId) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0)  throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Task task = taskRepostory.removeByIndex(index,userId);
        if(task==null) throw new TaskNotFoundException("индекс",userId,index.toString());
        return task;
    }

    /**
     * Удалить по идентификаотру
     * @param id идентификатор
     * @return задача
     */
    public Task removeById(final Long id) throws WrongArgumentException, TaskNotFoundException {
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.removeById(id);
        if(task==null) throw new TaskNotFoundException("ИД",id.toString());
        return task;
    }

    /**
     * Удалить по идентификаотру и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeById(final Long id,final Long userId) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.removeById(id,userId);
        if(task==null) throw new TaskNotFoundException("ИД",userId,id.toString());
        return task;
    }

    /**
     * Удалить по имени
     * @param name имя
     * @return задача
     */
    public Task removeByName(final String name) throws WrongArgumentException, TaskNotFoundException {
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.removeByName(name);
        if(task==null) throw new TaskNotFoundException("имя",name);
        return task;
    }

    /**
     * Удалить по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByName(final String name,final Long userId) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.removeByName(name,userId);
        if(task==null) throw new TaskNotFoundException("имя",userId,name);
        return task;
    }

    /**
     * Получить все задачи проекта
     * @param projectId projectId
     * @return список задач
     */
    public List<Task> findAllByProjectId(Long projectId) throws WrongArgumentException {
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId);
    }

    /**
     * Получить все задачи проекта с учетом пользователя
     * @param projectId projectId
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId,userId);
    }

    /**
     * Получить все задачи
     * @return список задач
     */
    public List<Task> findAll() {
        return taskRepostory.findAll();
    }

    /**
     * Получить все задачи пользователя
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAll(final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return taskRepostory.findAll(userId);
    }

    /**
     * Получить задачу по проекту и ид
     * @param projectId ид проекта
     * @param id ид
     * @return задача
     */
    public Task findByProjectIdAndId(Long projectId, Long id,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId, id);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("ИД",id.toString(),projectId);
        return task;
    }

    /**
     * Получить задачу по проекту ид и пользователю.
     * @param projectId ид проекта
     * @param id ид
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByProjectIdAndId(Long projectId, Long id, final Long userId,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId, id, userId);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("ИД",userId,id.toString(),projectId);
        return task;
    }

    public void saveAs(ObjectMapper objectMapper, OutputStream outputStream) throws IOException {
        taskRepostory.saveAs(objectMapper, outputStream);
    }

    public void loadFrom(ObjectMapper objectMapper, InputStream inputStream) throws IOException {
        taskRepostory.loadFrom(objectMapper, inputStream);
    }
}
