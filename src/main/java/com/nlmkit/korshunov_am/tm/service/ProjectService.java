package com.nlmkit.korshunov_am.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.entity.Project;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Сервис проектов
 */
public class ProjectService extends AbstractService {
    /**
     * Приватный конструктор по умолчанию
     */
    private ProjectService(){
        super();
        this.projectRepository = ProjectRepository.getInstance();
    }
    /**
     * конструктор для тестирования
     */
    public ProjectService(ProjectRepository projectRepository){
        super();
        this.projectRepository = projectRepository;
    }
    /**
     * Единственный экземпляр объекта ProjectService
     */
    private static ProjectService instance = null;

    /**
     * Получить единственный экземпляр объекта ProjectService
     * @return единственный экземпляр объекта ProjectService
     */
    public static ProjectService getInstance(){
        if (instance == null){
            instance = new ProjectService();
        }
        return instance;
    }
    /**
     * Репозитарий проектов.
     */
    private final ProjectRepository projectRepository;


    /**
     * Создать проект
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return проект
     */
    public Project create(final String name, final String description,final Long userId) throws WrongArgumentException {
        logger.trace("create({},{},{})",name,description,userId);
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание проекта");
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return projectRepository.create(name, description,userId);
    }

    /**
     * Изменить проект
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return проект
     */
    public Project update(final Long id, final String name, final String description,final Long userId) throws WrongArgumentException {
        logger.trace("update({},{},{},{})",id,name,description,userId);
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание проекта");
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return projectRepository.update(id, name, description,userId);
    }

    /**
     * Удалить все проекты
     */
    public void clear() {
        logger.trace("clear()");
        projectRepository.clear();
    }

    /**
     * Удалить все проекты пользователя
     * @param userId ид пользователя
     */
    public void clear(final Long userId) throws WrongArgumentException {
        logger.trace("clear({})",userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        projectRepository.clear(userId);
    }

    /**
     * Найти проект по индексу
     * @param index индекс
     * @return проект
     */
    public Project findByIndex(final Integer index,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException{
        logger.trace("findByIndex({},{})",index,throwIfNotFound);
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.findByIndex(index);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("индекс",index.toString());
        return project;
    }

    /**
     * Найти проект по индексу и пользователю
     * @param index индекс
     * @param userId ид пользователя
     * @return проект
     */
    public Project findByIndex(final Integer index,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("findByIndex({},{},{})",index,userId,throwIfNotFound);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.findByIndex(index,userId);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("индекс",index.toString());
        return project;
    }
    /**
     * Найти проект по итмени
     * @param name имя
     * @return проект
     */
    public Project findByName(final String name,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("findByName({},{})",name,throwIfNotFound);
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.findByName(name);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("имя",name);
        return project;
    }

    /**
     * Найти проект по итмени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return проект
     */
    public Project findByName(final String name,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("findByName({},{},{})",name,userId,throwIfNotFound);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.findByName(name,userId);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("имя",userId,name);
        return project;
    }

    /**
     * Найти проект про идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project findById(final Long id,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException  {
        logger.trace("findById({},{})",id,throwIfNotFound);
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.findById(id);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("ИД",id.toString());
        return project;
    }

    /**
     * Найти проект про идентификатору и пользователю
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект
     */
    public Project findById(final Long id,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException{
        logger.trace("findById({},{},{})",id,userId,throwIfNotFound);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.findById(id,userId);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("ИД",userId,id.toString());
        return project;
    }

    /**
     * Удалитьпроект по индексу
     * @param index индекс
     * @return проект
     */
    public Project removeByIndex(final Integer index) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeByIndex({})",index);
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.removeByIndex(index);
        if(project==null) throw new ProjectNotFoundException("индекс",index.toString());
        return project;
    }

    /**
     * Удалитьпроект по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeByIndex(final Integer index,final Long userId) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeByIndex({},{})",index,userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0)  throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.removeByIndex(index,userId);
        if(project==null) throw new ProjectNotFoundException("индекс",userId,index.toString());
        return project;
    }

    /**
     * Удалить проект по идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project removeById(final Long id) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeById({})",id);
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.removeById(id);
        if(project==null) throw new ProjectNotFoundException("ИД",id.toString());
        return project;
    }
    /**
     * Удалить проект по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeById(final Long id,final Long userId) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeById({},{})",id,userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.removeById(id,userId);
        if(project==null) throw new ProjectNotFoundException("ИД",userId,id.toString());
        return project;
    }

    /**
     * Удалить проект по имени
     * @param name имя
     * @return проект
     */
    public Project removeByName(final String name) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeByName({})",name);
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.removeByName(name);
        if(project==null) throw new ProjectNotFoundException("имя",name);
        return project;
    }

    /**
     * Удалить проект по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeByName(final String name,final Long userId) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeByName({},{})",name,userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.removeByName(name,userId);
        if(project==null) throw new ProjectNotFoundException("ИД",userId,name);
        return project;
    }

    /**
     * Получить все проекты
     * @return список проектов
     */
    public List<Project> findAll() {
        logger.trace("findAll()");
        return projectRepository.findAll();
    }

    /**
     * Получить все проекты пользователя
     * @param userId ид пользователя
     * @return список проектов
     */
    public List<Project> findAll(final Long userId) throws WrongArgumentException{
        logger.trace("findAll({})",userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return projectRepository.findAll(userId);
    }
    /**
     * Сохранить в поток
     * @throws IOException ошибка ввода вывода
     */
    public void saveAs(ObjectMapper objectMapper, OutputStream outputStream) throws IOException {
        projectRepository.saveAs(objectMapper, outputStream);
    }
    /**
     * Загрузить из потока
     * @throws IOException ошибка ввода вывода
     */
    public void loadFrom(ObjectMapper objectMapper, InputStream inputStream) throws IOException {
        projectRepository.loadFrom(objectMapper, inputStream);
    }
}
