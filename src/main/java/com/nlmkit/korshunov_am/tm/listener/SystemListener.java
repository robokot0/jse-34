package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.SystemService;



import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;

public class SystemListener extends AbstractListener implements Listener {
    /**
     * Сервис системных комманд
     */
    SystemService systemService;
    /**
     * Приватный конструктор по умолчанию
     */
    private SystemListener(){
        super(CommandHistoryService.getInstance());
        systemService = SystemService.getInstance();
        displayWelcome();
    }
    /**
     * Публичный конструктор для тестирования
     */
    public SystemListener(CommandHistoryService commandHistoryService,SystemService systemService) {
        super(commandHistoryService);
        this.systemService=systemService;
        displayWelcome();
    }
    /**
     * Единственный экземпляр объекта SystemListener
     */
    private static SystemListener instance = null;

    /**
     * Получить единственный экземпляр объекта SystemListener
     * @return единственный экземпляр объекта SystemListener
     */
    public static SystemListener getInstance(){
        if (instance == null){
            instance = new SystemListener();
        }
        return instance;
    }

    @Override
    public int notify(String command) {
        switch (command) {
            case SHORT_VERSION:
            case VERSION: return displayVersion();
            case SHORT_ABOUT:
            case ABOUT:return displayAbout();
            case SHORT_HELP:
            case HELP:return displayHelp();
            case SHORT_EXIT:
            case EXIT:return displayExit();
            case SHORT_LOAD_TEST_DATA:
            case LOAD_TEST_DATA:return loadTestDataAndShowResult();
            default:return -1;
        }
    }

    /**
     * Показать выход
     * @return 0 выполнено
     */
    public int displayExit(){
        System.out.println("Terminate program");
        return 0;
    }
    /**
     * Приглашение при входе в программу
     */
    public void displayWelcome(){
        System.out.println("**** WELCOME TO TASK MANAGER ****");
    }
    /**
     * Показать версию
     * @return 0 выполнено
     */
    public int displayVersion() {
        System.out.println("1.0.0");
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        logger.info("Short command name in brackets");
        logger.info("----Common commands:");
        logger.info("version - Display version (v)");
        logger.info("about - Display developer info (a)");
        logger.info("help - Display list of command (h)");
        logger.info("exit - Terminate console application (e)");
        logger.info("exit - Terminate console application (e)");
        logger.info("save-to-json - Save data to JSON (sj)");
        logger.info("save-to-xml - Save data to XML (sx)");
        logger.info("load-test-data - Load test data (ltd)");
        logger.info("load-from-json - Load data from JSON (lj)");
        logger.info("load-from-xml - Load data from XML (lx)");
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Показать информацию о разработчике
     * @return 0 выполнено без ошибок
     */
    public int displayAbout() {
        logger.info("Andrey Korshunov");
        logger.info("korshunov_am@nlmk.com");
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Подгрузить тестовые данные
     * @return 0 выполнено без ошибок
     */
    public int loadTestDataAndShowResult() {
        systemService.loadTestData();
        ShowResult("[OK]");
        return 0;
    }


}
