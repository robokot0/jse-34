package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.publisher.InputConsole;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class AbstractListener implements Listener {
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());
    /**
     * Сервис истории комманд
     */
    protected CommandHistoryService commandHistoryService;
    /**
     * Сервис истории комманд
     */
    InputConsole inputConsole;
    /**
     * Текущий пользователь
     */
    private User user=null;
    /**
     * Конструктор
     */
    public AbstractListener(CommandHistoryService commandHistoryService) {
        this.commandHistoryService = commandHistoryService;
    }
    /**
     * Получить текущего пользователя
     * @return текущий пользователь
     */
    public User getUser() {
        return user;
    }
    /**
     * Установить текущего пользователя
     * @param user пользователь
     */
    public void setUser(User user) {
        this.user = user;
    }
    /**
     * Указать объект из котрого получать параметры комманд
     *
     * @param inputConsole объект из котрого получать параметры комманд
     */
    @Override
    public void setConsole(InputConsole inputConsole) {
        this.inputConsole = inputConsole;
    }
    /**
     * Ввод строкового параметра
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public String enterStringCommandParameter(final String parameterName) throws WrongArgumentException {
        final String parametervalue = inputConsole.getNextInputString(parameterName,false);
        commandHistoryService.AddCommandParameterToLastCommand(parameterName,parametervalue);
        return parametervalue;
    }
    /**
     * Ввод строкового параметра пароля (без отображения в истории пароля)
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public String enterPasswordCommandParameter(final String parameterName) throws WrongArgumentException {
        final String parametervalue = inputConsole.getNextInputPassword(parameterName,false);
        commandHistoryService.AddCommandParameterToLastCommand(parameterName,"******");
        return parametervalue;
    }
    /**
     * Ввод int параметра
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public Integer enterIntegerCommandParameter(final String parameterName) throws WrongArgumentException {
        final String parametervalue = inputConsole.getNextInputString(parameterName,true);
        commandHistoryService.AddCommandParameterToLastCommand(parameterName,parametervalue);
        try {
            return Integer.parseInt(parametervalue);
        } catch (NumberFormatException e) {
            throw new WrongArgumentException("Failed to get Integer " + parameterName + " from " + parametervalue);
        }
    }
    /**
     * Ввод long параметра
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public Long enterLongCommandParameter(final String parameterName) throws WrongArgumentException {
        final String parametervalue = inputConsole.getNextInputString(parameterName,true);
        commandHistoryService.AddCommandParameterToLastCommand(parameterName,parametervalue);
        try {
            return Long.parseLong(parametervalue);
        } catch (NumberFormatException e) {
            throw new WrongArgumentException("Failed to get Long " + parameterName + " from " + parametervalue);
        }
    }
    /**
     * Показать результат исполнения комманды и добавить в историю
     * @param result результат
     */
    public void ShowResult(final String result){
        inputConsole.showMessage(result);
        commandHistoryService.AddCommandResultToLastCommand(result);
    }
    /**
     * Проверка что пользователь аутентифицировался
     * @return true пользовтаель аутентифицировался
     * пользователь не аутентифицировался
     */
    public boolean testAuthUser(){
        if (this.user==null){
            logger.info("Please auth: ");
            ShowResult("[FAIL]");
            return false;
        }
        return true;
    }

    /**
     * Проверка что пользователь аутентифицировался и это администратор
     * @return true пользовтаель аутентифицировался и это администратор false
     * пользователь не аутентифицировался или это не администратор
     */
    public boolean testAdminUser(){
        if(!testAuthUser())
            return false;
        if (this.user.getRole() != Role.ADMIN){
            logger.info("Please auth admin user: ");
            ShowResult("[FAIL]");
            return false;
        }
        return true;
    }


}
