package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.listener.Listener;

public interface Publisher {
    /**
     * Доабвить слушатель комманд в список оповещения
     * @param listener слушатель комманд
     */
    Publisher addListener(Listener listener);
    /**
     * Удалить слушателя комманд
     * @param listener слушатель комманд
     */
    void deleteListener(Listener listener);

    /**
     * Оповестить слушателей о комманде
     * @param command комманда
     * @throws WrongArgumentException неправильный аргумент
     */
    void notifyListener(String command) throws WrongArgumentException;
    /**
     * Цикл чтения и оповещения слушателей о комманде
     * @throws WrongArgumentException ошибка неправильная комманда
     */
    void readAndExecuteCommand() throws WrongArgumentException;
    /**
     * Установить слушателям и публикатору новый источник ввода данных
     */
    void setConsole(InputConsole inputConsole);

}
