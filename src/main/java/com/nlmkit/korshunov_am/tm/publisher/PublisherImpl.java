package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.listener.CommandHistoryListener;
import com.nlmkit.korshunov_am.tm.listener.Listener;

import java.util.ArrayList;
import java.util.List;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;

public class PublisherImpl implements Publisher {
    /**
     * Список Listener которых надо оповещать
     */
    List<Listener> listeners = new ArrayList<>();
    /**
     * Сервис истории комманд
     */
    InputConsole inputConsole;
    /**
     * Публичный конструктор
     */
    public PublisherImpl(InputConsole inputConsole) {
        this.setConsole(inputConsole);
        commandHistoryListener = CommandHistoryListener.getInstance();
    }
    /**
     * Публичный конструктор для тестирования
     */
    public PublisherImpl(InputConsole inputConsole,CommandHistoryListener commandHistoryListener) {
        this.setConsole(inputConsole);
        this.commandHistoryListener = commandHistoryListener;
    }

    CommandHistoryListener commandHistoryListener ;

    /**
     * Добавить слушателя команд
     * @param listener слушатель комманд
     * @return текущий объект
     */
    @Override
    public Publisher addListener(Listener listener) {
        if (!listeners.contains(listener)){
            listener.setConsole(this.inputConsole);
            listeners.add(listener);
        }
        return this;
    }

    /**
     * Удалить слушателя комманд
     * @param listener слушатель комманд
     */
    @Override
    public void deleteListener(Listener listener) {
        listeners.remove(listener);
    }

    /**
     * Послать слушателю новую комманду
     * @param command комманда
     */
    @Override
    public void notifyListener(String command) throws WrongArgumentException {
        boolean wasListened = false;
        for (Listener listener : listeners) {
            wasListened = (listener.notify(command) == 0) || wasListened;
        }
        if (!wasListened)
            throw new WrongArgumentException("Unknown command");
    }

    /**
     * Прочитать команду из консоли и послать слушателям
     */
    @Override
    public void readAndExecuteCommand() {
        String command = "";
        while (!(EXIT.equals(command) || SHORT_EXIT.equals(command))) {
            try {
                command = inputConsole.getNextInputString("command",true);
                commandHistoryListener.addCommandToHistory(command);
                this.notifyListener(command);
            } catch (MessageException e) {
                commandHistoryListener.ShowResult("[FAIL] "+e.getMessage());
            }
        }
    }

    /**
     * Установить слушателям и публикатору новый источник ввода данных
     * @param inputConsole новый источник ввода данных
     */
    @Override
    public void setConsole(InputConsole inputConsole) {
        this.inputConsole=inputConsole;
        for (Listener listener : listeners) {
            listener.setConsole(inputConsole);
        }
    }
}
