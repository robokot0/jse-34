package com.nlmkit.korshunov_am.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.nlmkit.korshunov_am.tm.entity.*;

import java.io.*;
import java.util.*;

public abstract class AbstractRepository <Entity extends Object & EntityName & EntityDescription & EntityId & EntityUserId> {
    /**
     * Сущности
     */
    protected SortedMap<String, List<Entity>> entityes = new TreeMap<>();
    /**
     * Создать сущность
     * @param entity entity для создания и сохравнения
     * @return созданный проект
     */
    public Entity create(Entity entity) {
        List<Entity> entityList = entityes.computeIfAbsent(entity.getName(), k -> new ArrayList<>());
        entityList.add(entity);
        return entity;
    }

    /**
     * Изменить сущность
     * @param id Идентификатор
     * @param name Имя
     * @param description Описание
     * @return проект
     */
    public Entity update(final Long id,final String name,final String description,final Long userId) {
        final Entity entity = findById(id);
        entity.setId(id);
        if(!name.equals(entity.getName())){
            List<Entity> entityListOld = entityes.get(entity.getName());
            if(entityListOld!=null) entityListOld.remove(entity);
            List<Entity> entityListNew = entityes.computeIfAbsent(name, k -> new ArrayList<>());
            entityListNew.add(entity);
        }
        entity.setName(name);
        entity.setDescription(description);
        entity.setUserId(userId);
        return entity;
    }

    /**
     * Очистить список сущностей
     */
    public void clear() {
        entityes.clear();
    }

    /**
     * Очистить список сущностей пользователя
     */
    public void clear(final Long userId) {
        entityes.forEach((entityName,entityList) -> entityList.removeIf(entity -> entity.getUserId().equals(userId)));
    }

    /**
     * Поиск сущности по индексу
     * @param index Индекс
     * @return проект если не найдено null
     */
    public Entity findByIndex(final int index){
        int currentIndex = 0;
        for (Map.Entry<String, List<Entity>> entry : entityes.entrySet()) {
            List<Entity> entityList = entry.getValue();
            if (index >= currentIndex + entityList.size())
                currentIndex += entityList.size();
            else
                return entityList.get(index-currentIndex);
        }
        return null;
    }

    /**
     * Поиск сущности по индексу и пользователю
     * @param index Индекс
     * @param userId ид пользователя
     * @return сущность если не найдено null
     */
    public Entity findByIndex(final int index,final Long userId){
        if (userId==null) return  null;
        int currentIndex = 0;
        for (Map.Entry<String, List<Entity>> entry : entityes.entrySet()) {
            List<Entity> entityList = entry.getValue();
            for(Entity entity:entityList){
                if (entity.getUserId().equals(userId)) {
                    if(currentIndex==index){
                        return entity;
                    }
                    currentIndex += 1;
                }
            }
        }
        return null;
    }

    /**
     * Поиск сущности по имени
     * @param name имя
     * @return проект если не найдено null
     */
    public Entity findByName(final String name){
        List<Entity> entityList = entityes.get(name);
        if(entityList!=null) {
            if (entityList.size() > 0) {
                return entityList.get(0);
            }
        }
        return null;
    }

    /**
     * Поиск сущности по имени и пользователю
     * @param name имя
     * @param userId ид пользователя
     * @return сущность если не найдено null
     */
    public Entity findByName(final String name,final Long userId){
        if (userId==null) return  null;
        List<Entity> entityList = entityes.get(name);
        if(entityList!=null) {
            for (final Entity entity : entityList)
                if (entity.getName().equals(name) && entity.getUserId().equals(userId)) {
                    return entity;
                }
        }
        return null;
    }

    /**
     * Поиск сущности по идентификатору
     * @param id идентификатор
     * @return сущность если не найдено null
     */
    public Entity findById(final Long id){
        for (Map.Entry<String, List<Entity>> entry : entityes.entrySet()) {
            List<Entity> enttyList = entry.getValue();
            for (final Entity entity: enttyList)
                if (entity.getId().equals(id)) return entity;
        }
        return null;
    }

    /**
     * Поиск сущности по идентификатору и пользователю
     * @param id идентификатор
     * @param userId ид пользователя
     * @return сущность если не найдено null
     */
    public Entity findById(final Long id,final Long userId){
        if (userId==null) return  null;
        final Entity entity = findById(id);
        if(entity!=null) {
            if (entity.getUserId().equals(userId)) {
                return entity;
            }
        }
        return null;
    }

    /**
     * Удалить сущность по индексу
     * @param index индекс
     * @return удаленная сущность если не найдено null
     */
    public Entity removeByIndex(final int index){
        final Entity entity = findByIndex(index);
        if (entity == null) return null;
        List<Entity> entityList = entityes.get(entity.getName());
        entityList.remove(entity);
        return entity;
    }

    /**
     * Удалить сущность по индексу и пользователю
     * @param index индекс
     * @param userId ид пользователя
     * @return удаленная сущность если не найдено null
     */
    public Entity removeByIndex(final int index,final Long userId){
        if (userId==null) return  null;
        final Entity entity = findByIndex(index,userId);
        if (entity == null) return null;
        List<Entity> entityList = entityes.get(entity.getName());
        entityList.remove(entity);
        return entity;
    }

    /**
     * Удалить сущность по идентификатору
     * @param id  идентификатор
     * @return удаленная сущность если не найдено null
     */
    public Entity removeById(final Long id){
        final Entity entity = findById(id);
        if (entity == null) return null;
        List<Entity> entityList = entityes.get(entity.getName());
        entityList.remove(entity);
        return entity;
    }

    /**
     * Удалить сущность по идентификатору и пользователю
     * @param id  идентификатор
     * @param userId ид пользователя
     * @return удаленная сущность если не найдено null
     */
    public Entity removeById(final Long id,final Long userId){
        if (userId==null) return  null;
        final Entity entity = findById(id,userId);
        if (entity == null) return null;
        List<Entity> entityList = entityes.get(entity.getName());
        entityList.remove(entity);
        return entity;
    }

    /**
     * Удалить сущность по имени
     * @param name имя
     * @return удаленная сущность если не найдено null
     */
    public Entity removeByName(final String name){
        final Entity entity = findByName(name);
        if (entity == null) return null;
        List<Entity> entityList = entityes.get(entity.getName());
        entityList.remove(entity);
        return entity;
    }

    /**
     * Удалить сущность по имени и пользователю
     * @param name имя
     * @param userId ид пользователя
     * @return удаленная сущность если не найдено null
     */
    public Entity removeByName(final String name,final Long userId){
        if (userId==null) return  null;
        final Entity entity = findByName(name,userId);
        if (entity == null) return null;
        List<Entity> entityList = entityes.get(entity.getName());
        entityList.remove(entity);
        return entity;
    }

    /**
     * Получить список всех сущностей
     * @return список сущностей
     */
    public List<Entity> findAll() {
        final List<Entity> result = new ArrayList<>();
        for (Map.Entry<String, List<Entity>> entry : entityes.entrySet()) {
            List<Entity> entityList = entry.getValue();
            result.addAll(entityList);
        }
        return result;
    }

    /**
     * Получить список всех сущностей пользователя
     * @param userId ид пользователя
     * @return список сущностей
     */
    public List<Entity> findAll(final Long userId) {
        final List<Entity> result = new ArrayList<>();
        for (Map.Entry<String, List<Entity>> entry : entityes.entrySet()) {
            List<Entity> entityList = entry.getValue();
            for (final Entity entity: entityList){
                if(entity.getUserId().equals(userId)) result.add(entity);
            }
        }
        return result;
    }

    /**
     * Получить количество сущностей в репозитарии
     * @return количество сущностей
     */
    public int size() {
        int result=0;
        for (Map.Entry<String, List<Entity>> entry : entityes.entrySet()) {
            List<Entity> entityList = entry.getValue();
            result+=entityList.size();
        }
        return result;
    }
    /**
     * Получить количество сущностей  пользователя в репозитарии
     * @param userId ид пользователя
     * @return количество сущностей
     */
    public int size(final Long userId) {
        int result=0;
        for (Map.Entry<String, List<Entity>> entry : entityes.entrySet()) {
            List<Entity> entityList = entry.getValue();
            for (final Entity entity: entityList)
                if(entity.getUserId().equals(userId))
                    result++;
        }
        return result;
    }
    public void saveAs(ObjectMapper objectMapper, OutputStream outputStream) throws IOException {
        objectMapper.setVisibility(PropertyAccessor.FIELD,JsonAutoDetect.Visibility.ANY);
        final List<Entity> entitiesAsList = new ArrayList<>();
        entityes.forEach((String key,List<Entity> entityList)-> entitiesAsList.addAll(entityList));
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(outputStream,entitiesAsList);
    }
    public abstract Class getEntityClass();
    public void loadFrom(ObjectMapper objectMapper, InputStream inputStream)  throws IOException {
        clear();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        JavaType listtype = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, getEntityClass());
        final List<Entity> entitiesAsList = objectMapper.readValue(inputStream, listtype);
        entitiesAsList.forEach(this::create);
    }


}
