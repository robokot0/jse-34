package com.nlmkit.korshunov_am.tm.repository;

import com.nlmkit.korshunov_am.tm.entity.Project;

/**
 * Репозитарий проектов
 */
public class ProjectRepository extends AbstractRepository <Project> {
    /**
     * Приватный конструктор по умолчанию
     */
    private ProjectRepository(){
    }
    /**
     * Единственный экземпляр объекта ProjectRepository
     */
    private static ProjectRepository instance = null;

    /**
     * Получить единственный экземпляр объекта ProjectRepository
     * @return единственный экземпляр объекта ProjectRepository
     */
    public static ProjectRepository getInstance(){
        if (instance == null){
            instance = new ProjectRepository();
        }
        return instance;
    }

    /**
     * Создать проект
     * @param name Имя проекта
     * @param userId ид пользователя
     * @return созданный проект
     */
    public Project create(final String name,final Long userId) {
        final Project project = new Project(name,userId);
        return create(project);
    }

    /**
     * Создать проект
     * @param name Имя
     * @param description Описание
     * @return созданный проект
     */
    public Project create(final String name,final String description,final Long userId) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return create(project);
    }
    public  Class getEntityClass(){return Project.class;}

}
